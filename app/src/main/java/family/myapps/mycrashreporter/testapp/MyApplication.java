package family.myapps.mycrashreporter.testapp;

import android.app.Application;

import family.myapps.mycrashreporter.MyCrashReporter;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MyCrashReporter.init(this);
    }
}
