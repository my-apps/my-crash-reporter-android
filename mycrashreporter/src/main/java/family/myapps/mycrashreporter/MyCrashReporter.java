package family.myapps.mycrashreporter;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class MyCrashReporter {

    private static String TAG = "MyCrashReporter";
    private static String CHANNEL_ID = "my-crash-reporter";

    public static void init(@NonNull final Context context) {
        final Thread.UncaughtExceptionHandler originalHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                Log.v(TAG, "Caught exception.", throwable);

                try {
                    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    // Create channel if using a supported SDK
                    if (Build.VERSION.SDK_INT >= 26) {
                        NotificationChannel channel = new NotificationChannel(
                                CHANNEL_ID,
                                "My Crash Reporter",
                                NotificationManager.IMPORTANCE_DEFAULT);
                        notificationManager.createNotificationChannel(channel);
                    }

                    // Build our pending intent to launch the crash activity
                    Intent intent = new Intent(context, DefaultCrashActivity.class);
                    intent.putExtra(Constants.KEY_THROWABLE, throwable);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 1 ,intent, PendingIntent.FLAG_CANCEL_CURRENT);

                    // Build and send the notification
                    Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("Crash!")
                            .setContentText("Crash Body!")
                            .setContentIntent(pendingIntent)
                            .build();
                    notificationManager.notify(90025, notification);
                } catch (Throwable t) {
                    Log.e(TAG, "Experienced an error in our error handling.", t);
                }

                originalHandler.uncaughtException(thread, throwable);
            }
        });
    }


}
