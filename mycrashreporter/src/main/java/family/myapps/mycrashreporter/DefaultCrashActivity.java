package family.myapps.mycrashreporter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */
public class DefaultCrashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_crash);

        if (getIntent() == null || getIntent().getExtras() == null || !getIntent().getExtras().containsKey(Constants.KEY_THROWABLE)) {
            throw new IllegalStateException("You must supply a Throwable in the bundle via Constants.KEY_THROWABLE");
        }

        // Show the full stacktrace
        Throwable throwable = (Throwable) getIntent().getExtras().getSerializable(Constants.KEY_THROWABLE);
        TextView crashText = findViewById(R.id.crash_text);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        throwable.printStackTrace(new PrintStream(outputStream));
        crashText.setText(new String(outputStream.toByteArray()));
    }
}
