package family.myapps.mycrashreporter;

/**
 * @author Greyson Parrelli (keybase.io/greyson)
 */

public class Constants {
    public static final String KEY_THROWABLE = "throwable";
}
